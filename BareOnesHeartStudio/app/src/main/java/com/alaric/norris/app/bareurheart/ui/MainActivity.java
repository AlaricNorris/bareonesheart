package com.alaric.norris.app.bareurheart.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.alaric.norris.app.bareurheart.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity {

    @InjectView ( R.id.toolbar )
    public Toolbar mToolbar;
    @InjectView ( R.id.fab )
    public FloatingActionButton mFloatingActionButton;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        ButterKnife.inject( this );
        mToolbar.setTitle( "神器" );
        setSupportActionBar( mToolbar );

        mFloatingActionButton.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick ( View view ) {
                        startActivity( new Intent( getApplicationContext(), OneActivity.class ) );
                    }
                }
        );
    }

}
