package com.alaric.norris.app.bareurheart.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.alaric.norris.app.bareurheart.BuildConfig;
import com.alaric.norris.app.bareurheart.R;
import com.alaric.norris.app.bareurheart.utils.BitmapCache;
import com.alaric.norris.app.bareurheart.utils.MediaPlay;
import com.alaric.norris.app.bareurheart.utils.SharedPreferencesXml;
import com.alaric.norris.app.bareurheart.utils.Util;
import com.alaric.norris.app.bareurheart.view.ColorPickerDialog;
import com.alaric.norris.app.bareurheart.view.SwitchButton;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdDisplayListener;
import com.startapp.android.publish.SDKAdPreferences;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;
import com.umeng.analytics.MobclickAgent;
import com.umeng.fb.FeedbackAgent;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ConfigActivity extends Activity {

    public int pic_kind = 1;
    public int music_kind = 2;
    public int first_back = 11;
    public int first_music = 12;
    AdView mAdView_Top, mAdView_Middle, mAdView_Bottom;
    @InjectView ( R.id.btn_cancel )
    ImageView mButton_Cancel;
    @InjectView ( R.id.btn_ok )
    ImageView mButton_OK;
    /**
     * StartAppAd object declaration
     */
    private StartAppAd mStartAppAd;
    /**
     * preference
     */
    private SharedPreferencesXml mSharedPreferencesXml;
    private Context mContext;
    private Button reset_bt;
    private EditText first_name_1, first_name_2;
    private Button first_back_bt, first_music_bt;
    private EditText second_et;
    private Button second_back_bt, second_enter_bt;
    private EditText thrid_f_et_1, thrid_f_et_2;
    private EditText thrid_s_et_1, thrid_s_et_2, thrid_s_et_3, thrid_s_et_4;
    private Button thrid_back_bt, thrid_music_bt;
    private SwitchButton music_on_off;
    private Button second_textcolor_bt;
    private int second_back = 21;
    private int thrid_back = 31;
    private int thrid_music = 32;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        new InitStartAppTask( savedInstanceState ).execute();
        setContentView( R.layout.config );
        ButterKnife.inject( this );
        mContext = getApplicationContext();
        Util.init().setContext( mContext );
        mSharedPreferencesXml = SharedPreferencesXml.init();
        MediaPlay.init().stop();
        findAllViews();
        setDefaultValues();
        createActions();
        if ( ! BuildConfig.DEBUG )
            setupAdView();
    }

    private void setupAdView () {
        // ADMob
        mAdView_Top = ( AdView ) findViewById( R.id.adview_top );
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView_Top.setAdListener( new AdListener() {

            @Override
            public void onAdClosed () {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad ( int errorCode ) {
                super.onAdFailedToLoad( errorCode );
                mAdView_Top.setVisibility( View.GONE );
            }

            @Override
            public void onAdLeftApplication () {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened () {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded () {
                super.onAdLoaded();
                mAdView_Top.setVisibility( View.VISIBLE );
            }
        } );
        mAdView_Top.loadAd( adRequest1 );
        // ADMob
        mAdView_Middle = ( AdView ) findViewById( R.id.adview_middle );
        AdRequest adRequest2 = new AdRequest.Builder().build();
        mAdView_Middle.setAdListener( new AdListener() {

            @Override
            public void onAdClosed () {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad ( int errorCode ) {
                super.onAdFailedToLoad( errorCode );
                mAdView_Middle.setVisibility( View.GONE );
            }

            @Override
            public void onAdLeftApplication () {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened () {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded () {
                super.onAdLoaded();
                mAdView_Middle.setVisibility( View.VISIBLE );
            }
        } );
        mAdView_Middle.loadAd( adRequest2 );
        // ADMob
        mAdView_Bottom = ( AdView ) findViewById( R.id.adview_bottom );
        AdRequest adRequest3 = new AdRequest.Builder().build();
        mAdView_Bottom.setAdListener( new AdListener() {

            @Override
            public void onAdClosed () {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad ( int errorCode ) {
                super.onAdFailedToLoad( errorCode );
                mAdView_Bottom.setVisibility( View.GONE );
            }

            @Override
            public void onAdLeftApplication () {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened () {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded () {
                super.onAdLoaded();
                mAdView_Bottom.setVisibility( View.VISIBLE );
            }
        } );
        mAdView_Bottom.loadAd( adRequest3 );
    }

    /**
     * Part of the activity's life cycle, StartAppAd should be integrated here.
     */
    @Override
    public void onResume () {
        super.onResume();
        if ( ! BuildConfig.DEBUG )
            mStartAppAd.onResume();
        MobclickAgent.onResume( this );
    }

    /**
     * Part of the activity's life cycle, StartAppAd should be integrated here
     * for the home button exit ad integration.
     */
    @Override
    public void onPause () {
        super.onPause();
        if ( ! BuildConfig.DEBUG )
            mStartAppAd.onPause();
        MobclickAgent.onPause( this );
    }

    /**
     * Part of the activity's life cycle, StartAppAd should be integrated here
     * for the back button exit ad integration.
     */
    @Override
    public void onBackPressed () {
        super.onBackPressed();
    }

    /**
     * @param inView
     */
    public void evaluate ( View inView ) {
        Uri mUri = Uri.parse( "market://details?id=" + getPackageName() );
        Intent mIntent = new Intent( Intent.ACTION_VIEW, mUri );
        mIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        try {
            startActivity( mIntent );
        }
        catch ( Exception e ) {
            e.printStackTrace();
            Toast.makeText( getApplicationContext(), "抱歉未检测到您手机安装的应用市场。。。", Toast.LENGTH_SHORT )
                 .show();
        }
    }

    public void share ( View inView ) {
        Intent sendIntent = new Intent();
        sendIntent.setAction( Intent.ACTION_SEND );
        sendIntent.setType( "text/*" );
        sendIntent.putExtra( Intent.EXTRA_TEXT, getString( R.string.share_our_app ) );
        startActivity( sendIntent );
    }

    public void feedback ( View inView ) {
        FeedbackAgent agent = new FeedbackAgent( this );
        agent.startFeedbackActivity();
    }

    /**
     * hideInput:()
     * ──────────────────────────────────
     *
     * @param inView
     * @version Ver 1.0
     * @since I used to be a programmer like you, then I took an arrow in the knee
     * ──────────────────────────────────────────────────────────────────────────────────────────────────────
     * Modified By 	AlaricNorris		 2014年8月9日下午9:51:27
     * Modifications:	TODO
     * ──────────────────────────────────────────────────────────────────────────────────────────────────────
     */
    public void hideInput ( View inView ) {
        try {
            InputMethodManager mInputMethodManager =
                    ( InputMethodManager ) getSystemService( INPUT_METHOD_SERVICE );
            mInputMethodManager.hideSoftInputFromWindow( inView.getWindowToken(), 0 );
        }
        catch ( Exception e ) {
        }
    }

    private void findAllViews () {
        reset_bt = ( Button ) findViewById( R.id.reset_bt );
        first_name_1 = ( EditText ) findViewById( R.id.first_et_1 );
        first_name_2 = ( EditText ) findViewById( R.id.first_et_2 );
        first_back_bt = ( Button ) findViewById( R.id.first_back_bt );
        first_music_bt = ( Button ) findViewById( R.id.first_music_bt );
        second_et = ( EditText ) findViewById( R.id.second_et );
        second_back_bt = ( Button ) findViewById( R.id.second_back_bt );
        second_enter_bt = ( Button ) findViewById( R.id.second_enter_bt );
        thrid_f_et_1 = ( EditText ) findViewById( R.id.thrid_f_et_1 ); //frist
        thrid_f_et_2 = ( EditText ) findViewById( R.id.thrid_f_et_2 );
        thrid_s_et_1 = ( EditText ) findViewById( R.id.thrid_s_et_1 ); //second
        thrid_s_et_2 = ( EditText ) findViewById( R.id.thrid_s_et_2 );
        thrid_s_et_3 = ( EditText ) findViewById( R.id.thrid_s_et_3 );
        thrid_s_et_4 = ( EditText ) findViewById( R.id.thrid_s_et_4 );
        thrid_back_bt = ( Button ) findViewById( R.id.thrid_back_bt );
        thrid_music_bt = ( Button ) findViewById( R.id.thrid_music_bt );
        music_on_off = ( SwitchButton ) findViewById( R.id.music_on_off );
        second_textcolor_bt = ( Button ) findViewById( R.id.second_textcolor_bt );
    }

    //初始化值
    private void setDefaultValues () {
        String fn1 = mSharedPreferencesXml.getConfigSharedPreferences( "first_name_1",
                                                                       R.string.first_et_1
        );
        String fn2 = mSharedPreferencesXml.getConfigSharedPreferences( "first_name_2",
                                                                       R.string.first_et_2
        );
        first_name_1.setText( fn1 );
        first_name_2.setText( fn2 );
        String second_words = mSharedPreferencesXml.getConfigSharedPreferences( "second_words",
                                                                                R.string.second_words
        );
        second_et.setText( second_words );
        thrid_f_et_1.setText( mSharedPreferencesXml.getConfigSharedPreferences( "thrid_f_et_1",
                                                                                R.string.thrid_f_et_1
        ) );
        thrid_f_et_2.setText( mSharedPreferencesXml.getConfigSharedPreferences( "thrid_f_et_2",
                                                                                R.string.thrid_f_et_2
        ) );
        thrid_s_et_1.setText( mSharedPreferencesXml.getConfigSharedPreferences( "thrid_s_et_1",
                                                                                R.string.thrid_s_et_1
        ) );
        thrid_s_et_2.setText( mSharedPreferencesXml.getConfigSharedPreferences( "thrid_s_et_2",
                                                                                R.string.thrid_s_et_2
        ) );
        thrid_s_et_3.setText( mSharedPreferencesXml.getConfigSharedPreferences( "thrid_s_et_3",
                                                                                R.string.thrid_s_et_3
        ) );
        thrid_s_et_4.setText( mSharedPreferencesXml.getConfigSharedPreferences( "thrid_s_et_4",
                                                                                R.string.thrid_s_et_4
        ) );
        MusicOn_off();
    }

    private void MusicOn_off () {
        String on_off = mSharedPreferencesXml.getConfigSharedPreferences( "music_on_off", "on" );
        if ( on_off.equals( "off" ) ) {
            music_on_off.setChecked( false );
        }
        else {
            music_on_off.setChecked( true, false );
        }
    }

    private void createActions () {
        first_back_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                Toast.makeText( mContext, R.string.configbackpic, Toast.LENGTH_SHORT ).show();
                setLoaclBackPicAndMu( pic_kind, first_back );
            }
        } );
        first_music_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                // TODO 自动生成的方法存根
                setLoaclBackPicAndMu( music_kind, first_music );
            }
        } );
        second_back_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                Toast.makeText( mContext, R.string.configbackpic, Toast.LENGTH_SHORT ).show();
                setLoaclBackPicAndMu( pic_kind, second_back );
            }
        } );
        thrid_back_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                Toast.makeText( mContext, R.string.configbackpic, Toast.LENGTH_SHORT ).show();
                setLoaclBackPicAndMu( pic_kind, thrid_back );
            }
        } );
        thrid_music_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                // TODO 自动生成的方法存根
                setLoaclBackPicAndMu( music_kind, thrid_music );
            }
        } );
        //换行
        second_enter_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                // TODO 自动生成的方法存根
                second_et.append( "\n" );
                //second_et.
            }
        } );
        music_on_off.setOnCheckedChangeListener( new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged ( CompoundButton buttonView, boolean isChecked ) {
                // TODO 自动生成的方法存根
                if ( isChecked ) {
                    mSharedPreferencesXml.setConfigSharedPreferences( "music_on_off", "on" );
                }
                else {
                    mSharedPreferencesXml.setConfigSharedPreferences( "music_on_off", "off" );
                }
            }
        } );
        reset_bt.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                // TODO 自动生成的方法存根
                new AlertDialog.Builder( ConfigActivity.this ).setIcon(
                        android.R.drawable.ic_dialog_alert )
                                                              .setTitle( R.string.dialog_title )
                                                              .setMessage( R.string.reset_message )
                                                              .setNegativeButton( R.string.cancle,
                                                                                  new DialogInterface.OnClickListener() {

                                                                                      @Override
                                                                                      public void onClick (
                                                                                              DialogInterface dialog,
                                                                                              int which
                                                                                      ) {
                                                                                      }
                                                                                  }
                                                              )
                                                              .setPositiveButton( R.string.ok,
                                                                                  new DialogInterface.OnClickListener() {

                                                                                      public void onClick (
                                                                                              DialogInterface dialog,
                                                                                              int whichButton
                                                                                      ) {
                                                                                          //初始化
                                                                                          mSharedPreferencesXml
                                                                                                  .setDefault();
                                                                                          //更新此界面
                                                                                          setDefaultValues();
                                                                                      }
                                                                                  }
                                                              )
                                                              .create()
                                                              .show();
            }
        } );
        mButton_Cancel.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                // TODO 自动生成的方法存根
                gotoFirstAc();
            }
        } );
        second_textcolor_bt.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick ( View v ) {
                int C = mContext.getResources().getColor( R.color.huang );
                String getc = mSharedPreferencesXml.getConfigSharedPreferences( "second_color",
                                                                                String.valueOf( C )
                );
                ColorPickerDialog dialog =
                        new ColorPickerDialog( ConfigActivity.this, Integer.valueOf( getc ),
                                               getResources().getString(
                                                       R.string.btn_color_picker ),
                                               new ColorPickerDialog.OnColorChangedListener() {

                                                   @Override
                                                   public void colorChanged ( int color ) {
                                                       mSharedPreferencesXml.setConfigSharedPreferences(
                                                               "second_color", String.valueOf(
                                                                       color ) );
                                                   }
                                               }
                        );
                dialog.show();
            }
        } );
        mButton_OK.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick ( View v ) {
                String f_1 = first_name_1.getText().toString().trim();
                String f_2 = first_name_2.getText().toString().trim();
                String s_s = second_et.getText().toString();
                String t_f_11 = thrid_f_et_1.getText().toString();
                String t_f_12 = thrid_f_et_2.getText().toString();
                String t_s_11 = thrid_s_et_1.getText().toString();
                String t_s_12 = thrid_s_et_2.getText().toString();
                String t_s_13 = thrid_s_et_3.getText().toString();
                String t_s_14 = thrid_s_et_4.getText().toString();
                mSharedPreferencesXml.setConfigSharedPreferences( "first_name_1", f_1 );
                mSharedPreferencesXml.setConfigSharedPreferences( "first_name_2", f_2 );
                mSharedPreferencesXml.setConfigSharedPreferences( "second_words", s_s );
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_f_et_1", t_f_11 );
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_f_et_2", t_f_12 );
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_s_et_1", t_s_11 );
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_s_et_2", t_s_12 );
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_s_et_3", t_s_13 );
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_s_et_4", t_s_14 );
                // Show an Ad
                if ( ! mStartAppAd.showAd( new AdDisplayListener() {

                    /**
                     * Callback when Ad has been hidden
                     * @param ad
                     */
                    @Override
                    public void adHidden ( Ad ad ) {
                        gotoFirstAc();
                    }

                    /**
                     * Callback when ad has been displayed
                     * @param ad
                     */
                    @Override
                    public void adDisplayed ( Ad ad ) {
                    }

                    /**
                     * Callback when ad has been clicked
                     * @param ad
                     */
                    @Override
                    public void adClicked ( Ad ad ) {
                    }

                    @Override
                    public void adNotDisplayed ( Ad ad ) {

                    }
                } ) ) {
                    gotoFirstAc();
                }
            }
        } );
    }

    //获取当前的版本名字
    private String getCurrentVersionName () throws NameNotFoundException {
        PackageManager packagemanager = mContext.getPackageManager();
        PackageInfo packageinfo = packagemanager.getPackageInfo( mContext.getPackageName(), 0 );
        return packageinfo.versionName;
    }

    public void gotoFirstAc () {
        Intent t = new Intent();
        t.setClass( ConfigActivity.this, OneActivity.class );
        startActivity( t );
        //overridePendingTransition(R.anim.slide_left_out,R.anim.slide_left_in);//小小动画
        overridePendingTransition( android.R.anim.slide_in_left, android.R.anim.slide_out_right );
        ConfigActivity.this.finish();
        BitmapCache.getInstance().clearCache(); //软引用，跳到新界面可清空原来图片内存
    }

    //本地图片按钮响应
    public void setLoaclBackPicAndMu ( int kind_type, int activity ) {
        //取消对话框
        Intent intent = new Intent();
        /* 开启Pictures画面Type设定为image */
        if ( kind_type == music_kind )
            intent.setType( "audio/*" );
        else
            intent.setType( "image/*" );
        /* 使用Intent.ACTION_GET_CONTENT这个Action */
        intent.setAction( Intent.ACTION_GET_CONTENT );
        /* 取得相片后返回本画面 */
        startActivityForResult( intent, activity );
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data ) {
        if ( resultCode == RESULT_OK ) {
            Uri uri = data.getData();
            Log.e( "uri", uri.toString() );
            if ( requestCode == first_back ) {
                mSharedPreferencesXml.setConfigSharedPreferences( "first_back", uri.toString() );
            }
            else if ( requestCode == first_music ) {
                mSharedPreferencesXml.setConfigSharedPreferences( "first_music", uri.toString() );
            }
            else if ( requestCode == second_back ) {
                mSharedPreferencesXml.setConfigSharedPreferences( "second_back", uri.toString() );
            }
            else if ( requestCode == thrid_back ) {
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_back", uri.toString() );
            }
            else if ( requestCode == thrid_music ) {
                mSharedPreferencesXml.setConfigSharedPreferences( "thrid_music", uri.toString() );
            }
        }
        super.onActivityResult( requestCode, resultCode, data );
    }

    @Override
    public boolean onKeyDown ( int keyCode, KeyEvent event ) {
        // TODO 自动生成的方法存根
        if ( keyCode == KeyEvent.KEYCODE_BACK ) {
            gotoFirstAc();
            return true;
        }
        else
            return super.onKeyDown( keyCode, event );
    }

    /**
     * 获取StartApp
     */
    private class InitStartAppTask extends AsyncTask< Void, Void, Void > {
        private Bundle mBundle;

        public InitStartAppTask ( Bundle mBundle ) {
            this.mBundle = mBundle;
        }

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground ( Void... params ) {
            mStartAppAd = new StartAppAd( ConfigActivity.this );
            // StartApp
            StartAppSDK.init( ConfigActivity.this, "102718341", "208088253",
                              new SDKAdPreferences().setAge( 20 )
                                                    .setGender( SDKAdPreferences.Gender.MALE )
            );
            return null;
        }

        /**
         * <p>Runs on the UI thread after {@link #doInBackground}. The
         * specified result is the value returned by {@link #doInBackground}.</p>
         * <p/>
         * <p>This method won't be invoked if the task was cancelled.</p>
         *
         * @param aVoid The result of the operation computed by {@link #doInBackground}.
         * @see #onPreExecute
         * @see #doInBackground
         * @see #onCancelled(Object)
         */
        @Override
        protected void onPostExecute ( Void aVoid ) {
            super.onPostExecute( aVoid );
            /** Create Splash Ad **/
            StartAppAd.showSplash( ConfigActivity.this, mBundle );
            /** Add Slider **/
            StartAppAd.showSlider( ConfigActivity.this );
        }
    }
}
