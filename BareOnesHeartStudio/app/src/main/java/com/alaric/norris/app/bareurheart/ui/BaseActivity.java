/**
 *  BaseActivity
 *  com.alaric.norris.app.bareurheart.ui
 * 	Function： 	${TODO}
 *  date            author
 *  ──────────────────────────────────
 *  2015/8/13      AlaricNorris
 *	Copyright (c) 2015, TNT All Rights Reserved.
 */
package com.alaric.norris.app.bareurheart.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.alaric.norris.app.bareurheart.R;
import com.alaric.norris.app.bareurheart.utils.BitmapCache;
import com.alaric.norris.app.bareurheart.utils.CloseAction;
import com.alaric.norris.app.bareurheart.utils.MediaPlay;
import com.alaric.norris.app.bareurheart.utils.SharedPreferencesXml;
import com.alaric.norris.app.bareurheart.utils.Util;
import com.alaric.norris.app.bareurheart.view.BaseSurfaceView;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;
import butterknife.InjectView;
/**
 *  ClassName:  BaseActivity
 *  Function:   base
 *  @author AlaricNorris
 *  @Contact Norris.sly@gmail.com
 *  @version Ver 1.0
 *  @since I used to be a programmer like you, then I took an arrow in the knee
 *  @Date 2015     2015/8/13     8:38
 *  @see
 *  ──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	@Fields
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	@Methods ${ENCLOSING_TYPE}
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 * 	Modified By 	AlaricNorris		 2015/8/138:38
 *	Modifications:	init
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 */
public abstract class BaseActivity extends Activity implements View.OnLongClickListener {

    public static final int MESSAGE_WHAT_PLAY_MUSIC = 5;
    protected Activity mActivity;
    protected int mInt_ScreenHeight;
    protected int mInt_ScreenWidth;
    protected SharedPreferencesXml mSharedPreferencesXml;
    protected MediaPlay mMediaPlay;
    protected BaseHandler mHandler;
    @InjectView ( R.id.frame )
    protected FrameLayout mFrameLayout;
    @InjectView ( R.id.linear )
    protected LinearLayout mLinearLayout;
    /**
     *
     */
    protected BaseSurfaceView mSurfaceView;
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.activity_base );
        initScreenInfo();
        mActivity = this;
        Util.init().setContext( mActivity.getApplicationContext() );
        mSharedPreferencesXml = SharedPreferencesXml.init();
        initMusicInfo();
        initHandler();
        initContentView();
        createActions();
    }
    protected void initScreenInfo () {
        Display localDisplay = getWindowManager().getDefaultDisplay();
        mInt_ScreenWidth = localDisplay.getWidth();
        mInt_ScreenHeight = localDisplay.getHeight();
    }
    protected void initHandler () {
        mHandler = new BaseHandler( this );
    }
    protected void initContentView () {
        ButterKnife.inject( this );
        mFrameLayout = ButterKnife.findById( this, R.id.frame );
        mLinearLayout = ButterKnife.findById( this, R.id.linear );
        initSurfaceView();
    }
    protected abstract void initSurfaceView ();

    protected void initMusicInfo () {
        //初始化音乐
        mMediaPlay = MediaPlay.init();
        loadMusic();
    }
    protected abstract void loadMusic ();

    protected abstract void createActions ();

    public void onResume () {
        super.onResume();
        MobclickAgent.onResume( this );
    }
    public void onPause () {
        super.onPause();
        MobclickAgent.onPause( this );
    }
    @Override
    public boolean onKeyDown ( int keyCode, KeyEvent event ) {
        if ( keyCode == KeyEvent.KEYCODE_BACK ) {
            new CloseAction( BaseActivity.this, mSurfaceView );
            return true;
        }
        else if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            mSurfaceView.setRun( true );
            Intent mIntent = new Intent();
            mIntent.setClass( BaseActivity.this, ConfigActivity.class );
            overridePendingTransition(
                    android.R.anim.slide_in_left, android.R.anim.slide_out_right
            );
            startActivity( mIntent );
            BaseActivity.this.finish();
            new Thread() {

                /**
                 * Calls the <code>run()</code> method of the Runnable object the receiver
                 * holds. If no Runnable is set, does nothing.
                 *
                 * @see Thread#start
                 */
                @Override
                public void run () {
                    BitmapCache.getInstance().clearCache(); //软引用，跳到新界面可清空原来图片内存
                }
            }.start();
            return true;
        }
        else
            return super.onKeyDown( keyCode, event );
    }

    /**
     * 使用静态的内部类，不会持有当前对象的引用
     */
    protected static class BaseHandler extends Handler {
        private final WeakReference< BaseActivity > mActivity;

        public BaseHandler ( BaseActivity activity ) {
            mActivity = new WeakReference< BaseActivity >( activity );
        }

        @Override
        public void handleMessage ( Message msg ) {
            BaseActivity activity = mActivity.get();
            if ( activity != null ) {
                // TODO
            }
        }
    }

}
