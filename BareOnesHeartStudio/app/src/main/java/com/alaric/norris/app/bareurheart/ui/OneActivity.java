package com.alaric.norris.app.bareurheart.ui;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.alaric.norris.app.bareurheart.R;
import com.alaric.norris.app.bareurheart.utils.BitmapCache;
import com.alaric.norris.app.bareurheart.utils.CloseAction;
import com.alaric.norris.app.bareurheart.utils.MediaPlay;
import com.alaric.norris.app.bareurheart.utils.SharedPreferencesXml;
import com.alaric.norris.app.bareurheart.utils.Util;
import com.alaric.norris.app.bareurheart.view.FirstSurfaceView;
import com.umeng.analytics.MobclickAgent;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class OneActivity extends Activity implements View.OnLongClickListener {

    private final int SHOWHEART = 2;

    private final int SHOWXIN = 1;

    private final int SHOWZI = 3;

    private final int GOTOSECOND = 4;

    private final int SOUND = 5;
    private final int NOCONFIG = 6;
    FrameLayout mFrameLayout;
    FirstSurfaceView mFirstSurfaceView;
    //static int CO;
    LinearLayout mLinearLayout;
    private Context mContext;
    private int screen_h;
    private int screen_w;
    private SharedPreferencesXml spxml;
    // private SoundPlay soundplay ;
    private MediaPlay me;
    Handler handler = new Handler() {

        public void handleMessage ( Message paramMessage ) {
            switch (paramMessage.what) {
                default:
                case SHOWXIN:
                    mFirstSurfaceView.showXin();
                    break;
                case SHOWHEART:
                    mFirstSurfaceView.showHeart();
                    break;
                case SHOWZI:
                    mFirstSurfaceView.showWenzi();
                    break;
                case GOTOSECOND:
                    goSecond();
                    break;
                case SOUND:
                    System.out.println( "palyaa" );
                    //soundplay.play(SoundPlay.HEARTV, 2);
                    String m_onoff = spxml.getConfigSharedPreferences( "music_on_off", "on" );
                    if ( ! m_onoff.equals( "off" ) )
                        me.play();
                    break;
            }
        }
    };

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.activity_base );
        Display localDisplay = getWindowManager().getDefaultDisplay();
        this.screen_w = localDisplay.getWidth();
        this.screen_h = localDisplay.getHeight();
        this.mContext = getApplicationContext();
        // CO = getResources().getColor(R.color.black);
        //    soundplay = SoundPlay.init();
        //    soundplay.setContext(mContext);
        //    soundplay.initSound();
        Util.init().setContext( mContext );
        spxml = SharedPreferencesXml.init();
        //初始化音乐
        me = MediaPlay.init();
        String first_music = spxml.getConfigSharedPreferences( "first_music", "0" );
        if ( first_music.equals( "" ) || first_music.equals( "0" ) )
            me.InitMediaPlay( mContext, R.raw.heartv );
        else
            me.InitMediaPlay( mContext, first_music );
        findAllViews();
        createActions();
    }

    public void onResume () {
        super.onResume();
        MobclickAgent.onResume( this );
    }

    public void onPause () {
        super.onPause();
        MobclickAgent.onPause( this );
    }

    private void findAllViews () {
        mFrameLayout = ( ( FrameLayout ) findViewById( R.id.frame ) );
        mLinearLayout = ( ( LinearLayout ) findViewById( R.id.linear ) );
    }

    private void createActions () {
        //this.mLinearLayout.setBackgroundColor(CO);
        //初始化背景
        String fb = spxml.getConfigSharedPreferences( "first_back", "0" );
        // 未设置背景图
        if ( fb.equals( "" ) || fb.equals( "0" ) )
            mLinearLayout.setBackgroundResource( R.drawable.q2 );
            // 读取配置好的背景图
        else {
            try {
                Drawable draw = null;
                Uri uri = Uri.parse( fb );
                ContentResolver cr = mContext.getContentResolver();
                InputStream in = cr.openInputStream( uri );
                Bitmap bitmap = Util.init().getBitmap( in );
                //ImageView imageView = (ImageView) findViewById(R.id.iv01);
                /* 将Bitmap设定到ImageView */
                //imageView.setImageBitmap(bitmap);
                draw = new BitmapDrawable( mContext.getResources(), bitmap );
                in.close();
                if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
                    mLinearLayout.setBackground( draw );
                else
                    mLinearLayout.setBackgroundDrawable( draw );
            }
            catch ( Exception e ) {
                mLinearLayout.setBackgroundResource( R.drawable.q2 );
            }
        }
        this.mFirstSurfaceView =
                new FirstSurfaceView( this, this.screen_w, this.screen_h, handler );
        this.mFrameLayout.removeAllViews();
        this.mFrameLayout.addView( this.mFirstSurfaceView, new ViewGroup.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        ) );
        this.mFirstSurfaceView.showBackground();
        this.mFirstSurfaceView.setOnLongClickListener( this );

        Action1< Integer > showAction = new Action1< Integer >() {

            @Override
            public void call ( Integer number ) {
                switch (number) {
                    case SHOWHEART:
                        mFirstSurfaceView.showHeart();
                        break;
                    case SOUND:
                        //soundplay.play(SoundPlay.HEARTV, 2);
                        String m_onoff = spxml.getConfigSharedPreferences( "music_on_off", "on" );
                        if ( ! m_onoff.equals( "off" ) )
                            me.play();
                        break;
                    case SHOWZI:
                        mFirstSurfaceView.showWenzi();
                        break;
                    case SHOWXIN:
                        mFirstSurfaceView.showXin();
                        break;
                }
                Log.i( "tag", "number" + number );
            }
        };
        Observable.OnSubscribe< Integer > observable1 = new Observable.OnSubscribe< Integer >() {

            @Override
            public void call ( Subscriber< ? super Integer > subscriber ) {
                try {
                    Thread.sleep( 2000 );
                }
                catch ( InterruptedException inE ) {
                    inE.printStackTrace();
                }
                subscriber.onNext( SHOWXIN );
                subscriber.onCompleted();
            }
        };
        Observable.create( observable1 )
                  .subscribeOn( Schedulers.io() )
                  .observeOn( AndroidSchedulers.mainThread() )
                  .subscribe( showAction );
        Observable.create( new Observable.OnSubscribe< Integer >() {

            @Override
            public void call ( Subscriber< ? super Integer > subscriber ) {
                try {
                    Thread.sleep( 1500L );
                }
                catch ( InterruptedException inE ) {
                    inE.printStackTrace();
                }
                subscriber.onNext( SHOWZI );
                subscriber.onCompleted();
            }
        } )
                  .subscribeOn( Schedulers.io() )
                  .observeOn( AndroidSchedulers.mainThread() )
                  .subscribe( showAction );
        Observable.OnSubscribe< Integer > observable2 = new Observable.OnSubscribe< Integer >() {

            @Override
            public void call ( Subscriber< ? super Integer > subscriber ) {
                try {
                    Thread.sleep( 1100 );
                }
                catch ( InterruptedException inE ) {
                    inE.printStackTrace();
                }
                subscriber.onNext( SOUND );
                subscriber.onCompleted();
            }
        };
        Observable.create( observable2 )
                  .delay( 1500, TimeUnit.MILLISECONDS )
                  .subscribeOn( Schedulers.io() )
                  .observeOn( AndroidSchedulers.mainThread() )
                  .subscribe( showAction );

        Observable.create( new Observable.OnSubscribe< Integer >() {

            @Override
            public void call ( Subscriber< ? super Integer > subscriber ) {
                try {
                    Thread.sleep( 1000L );
                }
                catch ( InterruptedException inE ) {
                    inE.printStackTrace();
                }
                subscriber.onNext( SHOWHEART );
                subscriber.onCompleted();
            }
        } )
                  .subscribeOn( Schedulers.io() )
                  .observeOn( AndroidSchedulers.mainThread() )
                  .subscribe( showAction );
        //mFirstSurfaceView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }

    public void goSecond () {
        startActivity( new Intent( OneActivity.this, TwoActivity.class ) );
        closeSelf();
        BitmapCache.getInstance().clearCache(); //软引用，跳到新界面可清空原来图片内存
    }

    @Override
    public boolean onKeyDown ( int keyCode, KeyEvent event ) {
        if ( keyCode == KeyEvent.KEYCODE_BACK ) {
            new CloseAction( OneActivity.this, mFirstSurfaceView );
            return true;
        }
        else if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            mFirstSurfaceView.setRun( true );
            startActivity( new Intent( OneActivity.this, ConfigActivity.class ) );
            closeSelf();
            BitmapCache.getInstance().clearCache(); //软引用，跳到新界面可清空原来图片内存
            return true;
        }
        else
            return super.onKeyDown( keyCode, event );
    }

    @Override
    public boolean onLongClick ( View v ) {
        mFirstSurfaceView.setRun( true );
        startActivity( new Intent( OneActivity.this, TwoActivity.class ) );
        closeSelf();
        return false;
    }
    private void closeSelf () {
        overridePendingTransition( android.R.anim.slide_in_left, android.R.anim.slide_out_right );
        finish();
    }

}