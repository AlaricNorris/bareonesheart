package com.alaric.norris.app.bareurheart.ui;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.alaric.norris.app.bareurheart.R;
import com.alaric.norris.app.bareurheart.utils.Util;
import com.alaric.norris.app.bareurheart.view.ThridSurfaceView;

import java.io.InputStream;

public class ThreeActivity extends BaseActivity {

    @Override
    protected void initSurfaceView () {
        mSurfaceView = new ThridSurfaceView(
                mActivity.getApplicationContext(), mInt_ScreenWidth, mInt_ScreenHeight
        );
        mSurfaceView.setOnLongClickListener( this );
    }
    @Override
    protected void loadMusic () {
        String music_path = mSharedPreferencesXml.getConfigSharedPreferences( "thrid_music", "0" );
        if ( music_path.equals( "" ) || music_path.equals( "0" ) )
            try {
                mMediaPlay.InitMediaPlay( mActivity.getApplicationContext(), R.raw.third_nan );
            }
            catch ( Exception e ) {
                e.printStackTrace();
            }
        else
            mMediaPlay.InitMediaPlay( mActivity.getApplicationContext(), music_path );

    }
    @Override
    protected void createActions () {
        {
            String tb = mSharedPreferencesXml.getConfigSharedPreferences( "thrid_back", "0" );
            if ( tb.equals( "" ) || tb.equals( "0" ) )
                mLinearLayout.setBackgroundResource( R.drawable.q5 );
            else {
                try {
                    Drawable draw = null;
                    Uri uri = Uri.parse( tb );
                    ContentResolver cr = mActivity.getContentResolver();
                    InputStream in = cr.openInputStream( uri );
                    Bitmap bitmap = Util.init().getBitmap( in );
                    //ImageView imageView = (ImageView) findViewById(R.id.iv01);
                /* 将Bitmap设定到ImageView */
                    //imageView.setImageBitmap(bitmap);
                    draw = new BitmapDrawable( mActivity.getResources(), bitmap );
                    in.close();
                    mLinearLayout.setBackgroundDrawable( draw );
                }
                catch ( Exception e ) {
                    //draw = getLocalDraw();
                    mLinearLayout.setBackgroundResource( R.drawable.q5 );
                }
            }
            //mLinearLayout.setBackgroundResource(R.drawable.q5);
            mFrameLayout.removeAllViews();
            mFrameLayout.addView(
                    mSurfaceView, new ViewGroup.LayoutParams(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT
                    )
            );
            mHandler.sendEmptyMessageDelayed( MESSAGE_WHAT_PLAY_MUSIC, 1100 );
        }
    }
    /**
     * Called when a view has been clicked and held.
     *
     * @param v The view that was clicked and held.
     *
     * @return true if the callback consumed the long click, false otherwise.
     */
    @Override
    public boolean onLongClick ( View v ) {
        return false;
    }
}
