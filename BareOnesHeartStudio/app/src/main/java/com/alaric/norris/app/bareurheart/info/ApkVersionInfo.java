package com.alaric.norris.app.bareurheart.info;

public class ApkVersionInfo {
    public static ApkVersionInfo downloadinfo = new ApkVersionInfo();
    private String VersionCode;
    private String ApkUrl;
    private String UpdataDescription;
    public String getVersionCode () {
        return VersionCode;
    }

    public void setVersionCode ( String versionCode ) {
        VersionCode = versionCode;
    }

    public String getApkUrl () {
        return ApkUrl;
    }

    public void setApkUrl ( String apkUrl ) {
        ApkUrl = apkUrl;
    }

    public String getUpdataDescription () {
        return UpdataDescription;
    }

    public void setUpdataDescription ( String updataDescription ) {
        UpdataDescription = updataDescription;
    }

}
