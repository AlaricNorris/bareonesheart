package com.alaric.norris.app.bareurheart.utils;
import android.util.Log;

import com.alaric.norris.app.bareurheart.BuildConfig;
public class DebugUtil {
    private static final String TAG = "nrs";

    /**
     * 开始追踪
     * @param customTag
     * @return
     */
    public static final long beginMonitor () {
        return beginMonitor( TAG );
    }

    /**
     * 开始追踪
     * @param customTag
     * @return
     */
    public static final long beginMonitor ( String customTag ) {
        if ( ! BuildConfig.DEBUG ) {
            return 0;
        }
        long begin = System.currentTimeMillis();
        Log.i( TAG, customTag + "begin:" + begin );
        return begin;
    }

    /**
     * 结束追踪
     * @param customTag
     * @return
     */
    public static final void endMonitor () {
        endMonitor( TAG );
    }

    /**
     * 结束追踪
     * @param customTag
     */
    public static final void endMonitor ( String customTag ) {
        if ( ! BuildConfig.DEBUG ) {
            return;
        }
        long end = System.currentTimeMillis();
        Log.i( TAG, customTag + "end:" + end );
    }

    /**
     * 开始追踪
     * @param customTag
     * @return
     */
    public static final void endMonitor ( long beginMillis ) {
        endMonitor( beginMillis, TAG );
    }

    /**
     * 结束追踪，并且
     * @param beginMillis
     * @param customTag
     */
    public static final void endMonitor ( long beginMillis, String customTag ) {
        if ( ! BuildConfig.DEBUG ) {
            return;
        }
        long end = System.currentTimeMillis();
        Log.i( TAG, customTag + "end:" + end );
        Log.i( TAG, customTag + "cost:" + ( end - beginMillis ) );
    }
}
