/**
 *  BaseSurfaceView
 *  com.alaric.norris.app.bareurheart.view
 * 	Function： 	${TODO}
 *  date            author
 *  ──────────────────────────────────
 *  2015/8/13      AlaricNorris
 *	Copyright (c) 2015, TNT All Rights Reserved.
 */
package com.alaric.norris.app.bareurheart.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.alaric.norris.app.bareurheart.interfaces.AllSurfaceView;
/**
 *  ClassName:  BaseSurfaceView
 *  Function:   ${TODO}  ADD FUNCTION
 *  Reason:     ${TODO}  ADD REASON
 *  @author AlaricNorris
 *  @contact Norris.sly@gmail.com
 *  @version Ver 1.0
 *  @since I used to be a programmer like you, then I took an arrow in the knee
 *  @Date 2015     2015/8/13     9:30
 *  @see        ${TAGS}
 *  ──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	@Fields
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	@Methods ${ENCLOSING_TYPE}
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 * 	Modified By 	AlaricNorris		 2015/8/139:30
 *	Modifications:	${TODO}
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 */
public abstract class BaseSurfaceView extends SurfaceView
        implements SurfaceHolder.Callback, AllSurfaceView {
    public BaseSurfaceView ( Context context ) {
        super( context );
    }
    public BaseSurfaceView ( Context context, AttributeSet attrs ) {
        super( context, attrs );
    }
    public BaseSurfaceView ( Context context, AttributeSet attrs, int defStyleAttr ) {
        super( context, attrs, defStyleAttr );
    }
}
